import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  todo: null,
  classNameBindings: ['isCompleted:completed'],
  isCompleted: null,
  actions: {
    update: function() {
      console.log(this.get('todo').get('title'))
      this.sendAction('isCompleted', this.get('todo'))
    }
  }
});
